import { Component } from "react";
import "./button.scss";

class Button extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isHover: false,
    };
  }
  render() {
    const { backgroundColor, text, onClick, id } = this.props;
    const { isHover } = this.state;

    const handleMouseEnter = () => {
      this.setState({ isHover: true });
    };

    const handleMouseLeave = () => {
      this.setState({ isHover: false });
    };

    return (
      <button
        id={id}
        className="button"
        style={{
          boxShadow: isHover ? "2px 2px 4px rgba(0, 0, 0, 0.6)" : "none",
          backgroundColor,
        }}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        onClick={onClick}
      >
        {text}
      </button>
    );
  }
}

export default Button;
