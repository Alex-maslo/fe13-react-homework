import React, { Component } from "react";
import "./ProductList.scss";

import ProductCard from "../product-card/ProductCard";

class ProductList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
    };
  }

  componentDidMount() {
    fetch("products.json")
      .then((response) => response.json())
      .then((data) => {
        this.setState({
          products: data,
        });
      });
  }

  render() {
    const { products } = this.state;

    return (
      <div className="product-list">
        {products.map((product) => (
          <ProductCard
            key={product.article}
            {...product}
            updateFavoriteCount={this.props.updateFavoriteCount}
          />
        ))}
      </div>
    );
  }
}

export default ProductList;
