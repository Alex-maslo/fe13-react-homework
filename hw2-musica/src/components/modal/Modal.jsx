import React, { Component } from "react";
import "./modal.scss";

class Modal extends Component {
  closeModal = () => {
    this.props.onClose();
  };

  handleOk = () => {
    this.props.addToCart();
    this.closeModal();
  };

  render() {
    const { header, text, footer, colorModal } = this.props;

    const modalButtonStyles = {
      fontSize: "20px",
      backgroundColor: "rgba(0,0,0, .3)",
      color: "white",
      padding: "10px",
      borderRadius: "5px",
      border: "none",
      width: "140px",
      height: "50px",
      cursor: "pointer",
    };

    const modalActions = [
      <button key="button1" onClick={this.handleOk} style={modalButtonStyles}>
        Ok
      </button>,
      <button key="button2" onClick={this.closeModal} style={modalButtonStyles}>
        Cancel
      </button>,
    ];

    return (
      <div
        className="modal__wrapper"
        onClick={this.closeModal}
        style={{ display: "block" }}
      >
        <div
          className="modal"
          style={colorModal}
          onClick={(e) => e.stopPropagation()}
        >
          <div className="modal__close-button" onClick={this.closeModal}>
            &times;
          </div>
          <div className="modal__header">{header}</div>
          <p className="modal__text">{text}</p>
          <p className="modal__text">{footer}</p>
          <div className="modal__button-container">{modalActions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
